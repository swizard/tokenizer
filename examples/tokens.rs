extern crate tokenizer;

use std::io::{stdin, BufRead, Error};
use tokenizer::Tokens;

fn main() {
    println!("Starting...");
    let stdin = stdin();
    let locked_stdin = stdin.lock();
    for maybe_line in locked_stdin.lines() {
        let line = maybe_line.unwrap();
        println!("Line: [{}]", line);
        for maybe_token in Tokens::new(line.chars().map(|c| Ok::<_, Error>(c))) {
            print!("{:?} ", maybe_token.unwrap());
        }
        println!("");
        println!("- - - - -");
    }
}
