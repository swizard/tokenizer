extern crate serde;
#[macro_use] extern crate serde_derive;

use std::fmt;
use std::iter::Iterator;
use std::marker::PhantomData;
use std::num::ParseIntError;

mod tables;
use tables::CharMisc;

#[derive(Clone, Copy)]
enum State {
    Free,
    PlainWord,
    MixedWord,
    NegativeNumberStart,
    IntegerNumber,
    MaybeFloatNumber(usize),
    FloatNumber,
    Punct,
    Emoji,
    Space(usize),
    Depleted,
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Number {
    Integer(String),
    Float(String),
    BigInteger(String),
}

impl fmt::Debug for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Number::Integer(ref value) => write!(f, "Integer({})", value),
            &Number::Float(ref value) => write!(f, "Float({})", value),
            &Number::BigInteger(ref value) => write!(f, "BigInteger({})", value),
        }
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Token {
    PlainWord(String),
    MixedWord(String),
    Number(Number),
    Punct(String),
    Emoji(String),
    Whitespaces(usize),
    Newline,
}

impl fmt::Debug for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Token::PlainWord(ref value) => write!(f, "PlainWord({})", value),
            &Token::MixedWord(ref value) => write!(f, "MixedWord({})", value),
            &Token::Number(ref value) => write!(f, "Number({:?})", value),
            &Token::Punct(ref value) => write!(f, "Punct({})", value),
            &Token::Emoji(ref value) => write!(f, "Emoji({})", value),
            &Token::Whitespaces(count) => write!(f, "Whitespaces({})", count),
            &Token::Newline => write!(f, "Newline"),
        }
    }
}

struct Window {
    buffer: Vec<char>,
    fallback: usize,
}

impl Window {
    fn new() -> Window { Window { buffer: Vec::new(), fallback: 0, } }

    fn fill(&mut self, ch: char) {
        self.buffer.push(ch);
    }

    fn clear(&mut self) {
        self.buffer.clear();
        self.fallback = 0;
    }

    fn have_unprocessed(&self) -> bool {
        self.fallback < self.buffer.len()
    }

    fn next_unprocessed(&mut self) -> Option<(usize, char)> {
        if self.have_unprocessed() {
            let result = (self.fallback, self.buffer[self.fallback]);
            self.fallback += 1;
            Some(result)
        } else {
            None
        }
    }

    fn fallback(&mut self, index: usize) {
        self.fallback = index;
    }

    fn flush(&mut self) -> String {
        let value = self.buffer[.. self.fallback].iter().cloned().collect();
        let rest = self.buffer.len() - self.fallback;
        for i in 0 .. rest {
            self.buffer[i] = self.buffer[i + self.fallback];
        }
        self.buffer.truncate(rest);
        self.fallback = 0;
        value
    }
}

pub struct Tokens<I, E> {
    chars: I,
    state: State,
    window: Window,
    _marker: PhantomData<E>,
}

impl<I, E> Tokens<I, E> where I: Iterator<Item = Result<char, E>> {
    pub fn new(chars: I) -> Tokens<I, E> {
        Tokens {
            chars: chars,
            state: State::Free,
            window: Window::new(),
            _marker: PhantomData,
        }
    }
}

impl<I, E> Iterator for Tokens<I, E> where I: Iterator<Item = Result<char, E>> {
    type Item = Result<Token, E>;

    fn next(&mut self) -> Option<Result<Token, E>> {
        loop {
            match self.state {
                State::Depleted =>
                    return None,
                _ if !self.window.have_unprocessed() => match self.chars.next() {
                    None => (),
                    Some(Ok(ch)) => self.window.fill(ch),
                    Some(Err(e)) => return Some(Err(e)),
                },
                _ =>
                    (),
            }

            match (self.state, self.window.next_unprocessed()) {
                (State::Depleted, _) =>
                    unreachable!(),
                // State::Free -> ..
                (State::Free, None) =>
                    self.state = State::Depleted,
                (State::Free, Some((_, ch))) if ch.is_alphabetic() =>
                    self.state = State::PlainWord,
                (State::Free, Some((_, '-'))) =>
                    self.state = State::NegativeNumberStart,
                (State::Free, Some((_, '\n'))) =>
                    return Some(Ok(Token::Newline)),
                (State::Free, Some((_, ch))) if ch.is_digit(10) =>
                    self.state = State::IntegerNumber,
                (State::Free, Some((_, ch))) if ch.is_numeric() =>
                    self.state = State::MixedWord,
                (State::Free, Some((_, ch))) if ch.is_punct() =>
                    self.state = State::Punct,
                (State::Free, Some((_, ch))) if ch.is_emoji() =>
                    self.state = State::Emoji,
                (State::Free, Some((_, ch))) if ch.is_whitespace() => {
                    self.window.clear();
                    self.state = State::Space(1);
                },
                (State::Free, _) =>
                    self.window.clear(),
                // State::PlainWord -> ..
                (State::PlainWord, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::PlainWord(self.window.flush())));
                },
                (State::PlainWord, Some((_, ch))) if ch.is_alphabetic() =>
                    (),
                (State::PlainWord, Some((_, ch))) if ch.is_numeric() =>
                    self.state = State::MixedWord,
                (State::PlainWord, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::PlainWord(self.window.flush())));
                },
                // State::MixedWord -> ..
                (State::MixedWord, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::MixedWord(self.window.flush())));
                },
                (State::MixedWord, Some((_, ch))) if ch.is_alphanumeric() =>
                    (),
                (State::MixedWord, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::MixedWord(self.window.flush())));
                },
                // State::NegativeNumberStart -> ..
                (State::NegativeNumberStart, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::Punct(self.window.flush())));
                },
                (State::NegativeNumberStart, Some((_, ch))) if ch.is_digit(10) =>
                    self.state = State::IntegerNumber,
                (State::NegativeNumberStart, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::Punct(self.window.flush())));
                },
                // State::IntegerNumber -> ..
                (State::IntegerNumber, None) => {
                    self.state = State::Depleted;
                    let value_string = self.window.flush();
                    return Some(Ok(Token::Number(match value_string.parse::<i64>() {
                        Ok(..) => Number::Integer(value_string),
                        Err(ParseIntError { .. }) => Number::BigInteger(value_string),
                    })));
                },
                (State::IntegerNumber, Some((_, ch))) if ch.is_digit(10) =>
                    (),
                (State::IntegerNumber, Some((_, ch))) if ch.is_alphabetic() =>
                    self.state = State::MixedWord,
                (State::IntegerNumber, Some((offset, '.'))) =>
                    self.state = State::MaybeFloatNumber(offset),
                (State::IntegerNumber, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    let value_string = self.window.flush();
                    return Some(Ok(Token::Number(match value_string.parse::<i64>() {
                        Ok(..) => Number::Integer(value_string),
                        Err(ParseIntError { .. }) => Number::BigInteger(value_string),
                    })));
                }
                // State::MaybeFloatNumber -> ..
                (State::MaybeFloatNumber(..), Some((_, ch))) if ch.is_digit(10) =>
                    self.state = State::FloatNumber,
                (State::MaybeFloatNumber(fallback), None) | (State::MaybeFloatNumber(fallback), Some(..)) => {
                    self.window.fallback(fallback);
                    self.state = State::Free;
                    let value_string = self.window.flush();
                    return Some(Ok(Token::Number(match value_string.parse::<i64>() {
                        Ok(..) => Number::Integer(value_string),
                        Err(ParseIntError { .. }) => Number::BigInteger(value_string),
                    })));
                },
                // State::FloatNumber -> ..
                (State::FloatNumber, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::Number(Number::Float(self.window.flush()))));
                },
                (State::FloatNumber, Some((_, ch))) if ch.is_digit(10) =>
                    (),
                (State::FloatNumber, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::Number(Number::Float(self.window.flush()))));
                }
                // State::Punct -> ..
                (State::Punct, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::Punct(self.window.flush())));
                },
                (State::Punct, Some((_, ch))) if ch.is_punct() =>
                    (),
                (State::Punct, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::Punct(self.window.flush())));
                }
                // State::Emoji -> ..
                (State::Emoji, None) => {
                    self.state = State::Depleted;
                    return Some(Ok(Token::Emoji(self.window.flush())));
                },
                (State::Emoji, Some((_, ch))) if ch.is_emoji() =>
                    (),
                (State::Emoji, Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::Emoji(self.window.flush())));
                }
                // State::Space -> ..
                (State::Space(count), None) => {
                    self.window.clear();
                    self.state = State::Depleted;
                    return Some(Ok(Token::Whitespaces(count)));
                },
                (State::Space(count), Some((_, ch))) if ch.is_whitespace() => {
                    self.window.clear();
                    self.state = State::Space(count + 1);
                },
                (State::Space(count), Some((offset, _))) => {
                    self.window.fallback(offset);
                    self.state = State::Free;
                    return Some(Ok(Token::Whitespaces(count)));
                },
            }
        }
    }
}
