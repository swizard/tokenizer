
pub trait CharMisc {
    fn is_punct(&self) -> bool;
    fn is_emoji(&self) -> bool;
}

impl CharMisc for char {
    fn is_punct(&self) -> bool {
        let table: &[u32] = &[
            0x00000021 /* EXCLAMATION_MARK */,
            0x00000022 /* QUOTATION_MARK */,
            0x00000023 /* NUMBER_SIGN */,
            0x00000024 /* DOLLAR_SIGN */,
            0x00000025 /* PERCENT_SIGN */,
            0x00000026 /* AMPERSAND */,
            0x00000027 /* APOSTROPHE */,
            0x00000028 /* LEFT_PARENTHESIS */,
            0x00000029 /* RIGHT_PARENTHESIS */,
            0x0000002A /* ASTERISK */,
            0x0000002B /* PLUS_SIGN */,
            0x0000002C /* COMMA */,
            0x0000002D /* HYPHEN-MINUS */,
            0x0000002E /* FULL_STOP */,
            0x0000002F /* SOLIDUS */,
            0x0000003A /* COLON */,
            0x0000003B /* SEMICOLON */,
            0x0000003C /* LESS-THAN_SIGN */,
            0x0000003D /* EQUALS_SIGN */,
            0x0000003E /* GREATER-THAN_SIGN */,
            0x0000003F /* QUESTION_MARK */,
            0x00000040 /* COMMERCIAL_AT */,
            0x0000005B /* LEFT_SQUARE_BRACKET */,
            0x0000005C /* REVERSE_SOLIDUS */,
            0x0000005D /* RIGHT_SQUARE_BRACKET */,
            0x0000005E /* CIRCUMFLEX_ACCENT */,
            0x0000005F /* LOW_LINE */,
            0x00000060 /* GRAVE_ACCENT */,
            0x0000007B /* LEFT_CURLY_BRACKET */,
            0x0000007C /* VERTICAL_LINE */,
            0x0000007D /* RIGHT_CURLY_BRACKET */,
            0x0000007E /* TILDE */,
            0x000000A1 /* INVERTED_EXCLAMATION_MARK */,
            0x000000A2 /* CENT_SIGN */,
            0x000000A3 /* POUND_SIGN */,
            0x000000A4 /* CURRENCY_SIGN */,
            0x000000A5 /* YEN_SIGN */,
            0x000000A6 /* BROKEN_BAR */,
            0x000000A7 /* SECTION_SIGN */,
            0x000000A8 /* DIAERESIS */,
            0x000000A9 /* COPYRIGHT_SIGN */,
            0x000000AA /* FEMININE_ORDINAL_INDICATOR */,
            0x000000AB /* LEFT-POINTING_DOUBLE_ANGLE_QUOTATION_MARK */,
            0x000000AC /* NOT_SIGN */,
            0x000000AD /* SOFT_HYPHEN */,
            0x000000AE /* REGISTERED_SIGN */,
            0x000000AF /* MACRON */,
            0x000000B0 /* DEGREE_SIGN */,
            0x000000B1 /* PLUS-MINUS_SIGN */,
            0x000000B5 /* MICRO_SIGN */,
            0x000000B6 /* PILCROW_SIGN */,
            0x000000B7 /* MIDDLE_DOT */,
            0x000000B8 /* CEDILLA */,
            0x000000BA /* MASCULINE_ORDINAL_INDICATOR */,
            0x000000BB /* RIGHT-POINTING_DOUBLE_ANGLE_QUOTATION_MARK */,
            0x000000BF /* INVERTED_QUESTION_MARK */,
            0x000000D7 /* MULTIPLICATION_SIGN */,
            0x000000F7 /* DIVISION_SIGN */,
            0x00000192 /* LATIN_SMALL_LETTER_F_WITH_HOOK */,
            0x000002C6 /* MODIFIER_LETTER_CIRCUMFLEX_ACCENT */,
            0x00002013 /* EN_DASH */,
            0x00002014 /* EM_DASH */,
            0x00002018 /* LEFT_SINGLE_QUOTATION_MARK */,
            0x00002019 /* RIGHT_SINGLE_QUOTATION_MARK */,
            0x0000201A /* SINGLE_LOW-9_QUOTATION_MARK */,
            0x0000201C /* LEFT_DOUBLE_QUOTATION_MARK */,
            0x0000201D /* RIGHT_DOUBLE_QUOTATION_MARK */,
            0x0000201E /* DOUBLE_LOW-9_QUOTATION_MARK */,
            0x00002020 /* DAGGER */,
            0x00002021 /* DOUBLE_DAGGER */,
            0x00002022 /* BULLET */,
            0x00002026 /* HORIZONTAL_ELLIPSIS */,
            0x00002030 /* PER_MILLE_SIGN */,
            0x00002039 /* SINGLE_LEFT-POINTING_ANGLE_QUOTATION_MARK */,
            0x0000203A /* SINGLE_RIGHT-POINTING_ANGLE_QUOTATION_MARK */,
            0x000020AB /* DONG_SIGN */,
            0x000020AC /* EURO_SIGN */,
            0x00002116 /* NUMERO_SIGN */,
            0x00002122 /* TRADE_MARK_SIGN */,
        ];

        table.binary_search(&(*self as u32)).is_ok()
    }

    fn is_emoji(&self) -> bool {
        match *self as u32 {
            0x1F600 ... 0x1F64F | // Emoticons
            0x1F300 ... 0x1F5FF | // Misc Symbols and Pictographs
            0x1F680 ... 0x1F6FF | // Transport and Map
            0x1F1E6 ... 0x1F1FF | // Regional country flags
            0x2600  ... 0x26FF  | // Misc symbols
            0x2700  ... 0x27BF  | // Dingbats
            0xFE00  ... 0xFE0F  | // Variation Selectors
            0x1F900 ... 0x1F9FF | // Supplemental Symbols and Pictographs
            8400    ... 8447   => // Combining Diacritical Marks for Symbols
                true,
            _ =>
                false,
        }
    }
}
